# A centrailized processing system

from Small_Zombie import Small_Zombie
import asyncio
import multiprocessing
from Server import Server
from colorlog import ColoredFormatter
import logging
import time

def Logger(name):
    formatter = ColoredFormatter(
    "%(name)s: %(log_color)s%(levelname)s - %(white)s%(asctime)s: %(log_color)s%(message)-10s ",
    datefmt=None,
    reset=True,
    log_colors={
    'DEBUG':    'cyan',
    'INFO':     'white',
    'WARNING':  'yellow',
    'ERROR':    'red',
    'CRITICAL': 'red',
    },

    style='%'
    )
    if name:
        name = name
    else:
        name = "Default"
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.addHandler(handler)
    return logger

def worker(worker_id, job_queue, output_queue):
    job = None
    while True:
        try:
            if job:
                output_queue.put([identifier, results])
                job = None
            else:
                if not job_queue.empty():
                    job = job_queue.get()
                    output_queue.put([identifier, results])
                    job = None
            time.sleep(0.1)
        except Exception as E:
            pass


def Server(job_queue, output_queue):
    server = Server_one_process(job_queue,output_queue)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(server.start_server())

if __name__ == "__main__":
    initiate_databases()
    workers = []
    worker_number = 5
    job_queue = multiprocessing.Queue()
    output_queue = multiprocessing.Queue()
    work_status = multiprocessing.Queue()
    for i in range(1, worker_number+1):
        worker_process = multiprocessing.Process(target=worker, args=(i,job_queue, output_queue))
        worker_process.start()
        workers.append(worker_process)
    server_process = multiprocessing.Process(target=Server, args=(job_queue,output_queue))
    server_process.start()


