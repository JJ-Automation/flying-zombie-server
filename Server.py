# A partial implementation for Flying Zombie server
# Code from scratch with asyncio and socket

import json
import asyncio
import socket
import re
import aiomysql
from datetime import datetime, timedelta
from colorlog import ColoredFormatter
import logging

def Logger(name):
    formatter = ColoredFormatter(
    "%(name)s - %(log_color)s%(levelname)s - %(white)s%(asctime)s: %(log_color)s%(message)-10s ",
    datefmt=None,
    reset=True,
    log_colors={
    'DEBUG':    'cyan',
    'INFO':     'white',
    'WARNING':  'yellow',
    'ERROR':    'red',
    'CRITICAL': 'red',
    },
    style='%'
    )
    if name:
        name = name
    else:
        name = "Default"
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.addHandler(handler)
    return logger

class Server:
    def __init__(self, job_queue, output_queue):
        self.task = None
        self.routes = ["/call", "/S"]
        self.loop = asyncio.get_event_loop()
        self.job_queue = job_queue
        self.output_queue = output_queue
        self.logger = Logger("Server")
        self.logger.setLevel(logging.CRITICAL)
        self.identifier = 0

    async def debug_(self, text):
        self.logger.debug(text)
    async def critical_(self, text):
        self.logger.critical(text)
    async def error(self, text):
        self.logger.error(text)

    async def generate_http_response(self, status_code, message, body=None):
        status_code = status_code
        status_line = f"HTTP/1.1 {status_code} OK Request\r\n"
        headers = "Content-Type: application/json\r\n"
        response_data = {'Message': body}
        response_body = json.dumps(response_data)
        headers += f"Content-Length: {len(response_body)}\r\n"
        response = status_line + headers + "\r\n" + response_body
        await self.debug_("Response Generated.")
        return response

    async def message_sending(self, client_socket, results):
        # partial
        if isinstance(results, int):
            if results == 401:
                response_data = {'Message': "Request parsing error, check your format."}
                response = await self.generate_http_response(401, "bad", response_data)
            elif results == 499:
                response_data = {'Message': "You are thaaaat close to be blocked."}
                response = await self.generate_http_response(400, "bad", response_data)
            elif results == 500:
                # Identified attacks, and sending an insulting message.
                response_data = {'Message': "Blocked, Sucker."}
                response = await self.generate_http_response(400, "bad", response_data)
            elif results == 403:
                response_data = {'Message': "request error"}
                response = await self.generate_http_response(403, "bad", response_data)
            elif results == 404:
                response_data = {'Message': "request error"}
                response = await self.generate_http_response(400, "bad", response_data)
            elif results == 405:
                response_data = {'Message': "proxy setting parsing error"}
                response = await self.generate_http_response(403, "bad", response_data)
            elif results == 407:
                response_data = {'Message': "request error"}
                response = await self.generate_http_response(400, "bad", response_data)
            else:
                response_data = {'Message': "400"}
                response = await self.generate_http_response(400, "bad", response_data)
            # Simple implementation sending rejection message, checking client connection if closed.
            try:
                await self.debug_(f"Sending back: Bad Request.{results}")
                await self.loop.sock_sendall(client_socket, response.encode())
                client_socket.close()
            except:
                client_socket.close()
        elif isinstance(results, list):
            # Displaying partial example
            if signal == 200:
                response_data = {"signal": signal,
                                 "loading": "X",
                                 "timer" : timer,
                                 'html': html      
            response = await self.generate_http_response(signal,"OK", response_data)
            try:
                await self.debug_("Sending back: List")
                await self.loop.sock_sendall(client_socket, response.encode())
                client_socket.close()
            except:
                client_socket.close()

    async def failed_attempt(self, address):
            async with connection.cursor() as cursor:
                await cursor.execute("SELECT * FROM failed_attempts WHERE ip_address = %s AND failed_attempts > 30", (address,))
                row = await cursor.fetchone()
                if row:
                    await self.add_ip_to_blocked_database(address)
                    return 500
                else:
                    await cursor.execute("INSERT INTO failed_attempts (ip_address, failed_attempts) VALUES (%s, 1)",
                                         (address,))
                    return 499

    async def route_small_zombie(self, approved_message):
        # Displaying Partial
        if self.identifier == 5000:
            self.identifier = 1
        else:
            self.identifier += 1
        current_identifier = self.identifier
        quest = [current_identifier, username]
        self.job_queue.put(quest)
        while True:
            if not self.output_queue.empty():
                results_list = self.output_queue.get()
                if results_list[0] != current_identifier:
                    self.output_queue.put(results_list)
                else:
                    return results_list[1]
            else:
                await asyncio.sleep()

    async def package_control(self, client_socket, address, data_buffer):
        try:
            message = data_buffer.decode()
            header_raw, body = message.split('\r\n\r\n', 1)
            body = json.loads(body)
            headers_raw = header_raw.split('\r\n')
            call_method, route, aaa = headers_raw[0].split(" ")
            headers = headers_raw[1:]
        except json.JSONDecodeError as json_e:
            await self.debug_(f"Error decoding JSON body {json_e}")
            return 401
        except Exception as E:
            await self.debug_(f"Parsing message error {E}")
            return 401

        message = [call_method, address, route, headers, body]
        if route == "/x":
            signal = await self.route_X(address,message)
            return signal
        elif route == "/call/Authenticate":
            results_list = await self.route_small_zombie_authenticate(message)
            return results_list
        else:
            # Do something
            
    async def client_control(self, client_socket, address):
        async with aiomysql.connect() as connection:
            async with connection.cursor() as cursor:
                await cursor.execute("SELECT * FROM blocked_ips WHERE ip_address = %s", (address[0],))
                row = await cursor.fetchone()
                if row:
                    await self.message_sending(client_socket, 500)
                    return
                else:
                    pass 

        data_buffer = b""
        content_length = None
        while True:
            data = await self.loop.sock_recv(client_socket, 2000)
            data_buffer += data
            if content_length is None:
                content_length_match = re.search(b'Content-Length: (\d+)', data_buffer)
            if content_length is not None and len(data_buffer) >= content_length:
                break
            # close connection when exceeding 4,000 bytes for minimizing DDoS attacks.
            if len(data_buffer) > 4000:
                await self.error(f"Over 4000 buffer: {data_buffer}")
                await self.message_sending(client_socket, 403)

        results = await self.data_control(client_socket, address, data_buffer)
        # Release Memory, LOL
        data_buffer = None
        await self.message_sending(client_socket,results)

    async def create_databases(self):
            async with connection.cursor() as cursor:
                await cursor.execute("CREATE DATABASE IF NOT EXISTS X ")
                await cursor.execute("USE X")
                await cursor.execute(
                    "CREATE TABLE IF NOT EXISTS users ("
                    "id INT AUTO_INCREMENT PRIMARY KEY,"
                    "username VARCHAR(255) NOT NULL,"
                    "credits INT,"
                    "failed_login_time INT,"
                    "blocking BOOLEAN DEFAULT FALSE)"
                )
                await cursor.execute("CREATE DATABASE IF NOT EXISTS X")
                await cursor.execute("USE Y")
                await cursor.execute("""
                    CREATE TABLE IF NOT EXISTS blocked_ips (
                        id INT AUTO_INCREMENT PRIMARY KEY,
                        ip_address VARCHAR(45) NOT NULL,
                        blocked_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                    )
                """)
                await cursor.execute("CREATE DATABASE IF NOT EXISTS X")
                await cursor.execute("USE Z")
                await cursor.execute("""
                                CREATE TABLE IF NOT EXISTS failed_attempts (
                                    id INT AUTO_INCREMENT PRIMARY KEY,
                                    ip_address VARCHAR(45) NOT NULL,
                                    failed_attempts INT DEFAULT 0,
                                    last_attempt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                                )
                            """)

    async def start_server (self):
        await self.create_databases()
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(('127.0.0.1', 3999))
        server_socket.listen()
        server_socket.setblocking(False)
        await self.debug_("Server started.")
        while True:
            client_socket, address = await self.loop.sock_accept(server_socket)
            asyncio.create_task(self.client_control(client_socket, address))

